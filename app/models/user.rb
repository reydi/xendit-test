class User < ActiveRecord::Base
  def transactions
    Transaction.where('payee_id = ? or payer_id = ?',
                      self.id, self.id).
                      order('created_at DESC')
  end
end
