module V1
  class TransactionController < ApplicationController

    before_filter :check_header

    def index
      @transactions = Transaction.where('payee_id = ? or payer_id = ?',
                                        params[:user_id], params[:user_id]).
                                  order('created_at DESC')

      render json: @transactions.to_json
    end

    def create
      @transaction = Transaction.new()
      @transaction.payer_id = params[:payer_id]
      @transaction.payee_id = params[:payee_id]
      @transaction.amount = params[:amount]
      @transaction.description = params[:description]
      if @transaction.save!
        render json: @transactions.to_json, status: 200
      else
        render json: @transaction.errors, status: 500
      end
    end

    def update
      @transaction = Transaction.find(params[:transaction_id])
      @transaction.payer_id = params[:payer_id]
      @transaction.payee_id = params[:payee_id]
      @transaction.amount = params[:amount] if params[:amount]
      @transaction.description = params[:description] if params[:description]
      if @transaction.save!
        render json: @transactions.to_json, status: 200
      else
        render json: @transaction.errors, status: 500
      end
    end

    def destroy
      @transaction = Transaction.find(params[:transaction_id])
      if @transaction.destroy
        render json: @transactions.to_json, status: 200
      else
        render json: @transaction.errors, status: 500
      end
    end

    private

    def check_header
      render json: {
        message: 'sorry, you\'re not authorized!'
        }, :status => :unauthorized unless request.headers['Authentication'] == 'development'
    end
  end
end