class WelcomeController < ApplicationController
  def index

  end

  def show
    @user = get_user_from_xendit(user_id: params[:user_id])
    hashed_result = eval(@user)
    if hashed_result[:error_code]
      render text: hashed_result[:message]
    else
      render json: @user, status: 200
    end
  end

  private

  def get_user_from_xendit(user_id: '55e0961c2f9c1016008002fe')
    HTTP.headers('X-API-KEY': ENV['XENDIT_API_KEY']).
      get('http://xendit-user-service-staging.uppju9xwxz.us-west-2.elasticbeanstalk.com/users/' + user_id.to_s).to_s
  end

end