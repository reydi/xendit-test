// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require new-age.min
//= require_tree .

$(document).ready(function() {
  $("#welcome").on("ajax:success", function(e, data, status, xhr) {
    var person = JSON.parse(xhr.responseText);
    $("#result").text('We found him!' +
      person.first_name + ' ' +
      person.last_name +
      ' has a phone number which is ' +
      person.phone_number +
      '. Call him now!');
  }).on("ajax:error", function(e, data, status, xhr) {
    $("#result").text(data.responseText);
  });
});
