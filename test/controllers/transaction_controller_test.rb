require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest

  test "should return a valid transaction data" do
    get v1_user_transaction_index_url(1), headers: {"Authentication" => "development"}
    assert_response :success
    assert @response.body.length, 1
  end
end
