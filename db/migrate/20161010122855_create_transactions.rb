class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :payer_id, null: false
      t.integer :payee_id, null: false
      t.float :amount, default: 0
      t.string :description

      t.timestamps null: false
    end
  end
end
