class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.decimal :initial_deposit, default: 0
      t.decimal :current_balance, default: 0
      t.string :email, null: false
      t.string :img_url

      t.timestamps null: false
    end
  end
end
